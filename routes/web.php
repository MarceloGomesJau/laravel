<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});




Route::get('/sistemabanda', function () {
    return view('login');
});


Route::get('/index', ['as' => 'index.sistema', 'uses' => 'IndexController@mostrar']);
//Usuario
Route::post('/login', ['as' => 'login', 'uses' => 'UsuarioController@login']);

Route::get('/loginSair', ['as' => 'login.sair', 'uses' => 'UsuarioController@sair']);

Route::post('validarNome', ['uses'=>'UsuarioController@validarNome']);

Route::post('validarEmail', ['uses'=>'UsuarioController@validarEmail']);

Route::post('/salvarUsuario',['as'=>'usuario.salvar', 'uses'=>'UsuarioController@salvar']);

Route::get('/cadastrarUsuario', ['as'=>'usuario.listar', 'uses'=>'UsuarioController@listarUsuario'] );

Route::get('/usuarioEditar/{codUser}', ['as'=>'usuario.editar', 'uses'=>'UsuarioController@editarUsuario']);

Route::put('/salvarEdicao/{codUser}', ['as'=>'usuario.update', 'uses'=>'UsuarioController@salvarEdicao']);

Route::get('excluirUsuario/{codUser}', 'UsuarioController@excluirUsuario');

//Fecha Usuario


//Banda artista

Route::post('/salvarArtista',['as'=>'banda_artista.salvar', 'uses'=>'BandaArtistaController@salvar']);

Route::post('/validarBanda',['uses'=> 'BandaArtistaController@validarBandaArtista']);

Route::post('visualizarBanda',['uses'=> 'BandaArtistaController@visualizarBanda']);

Route::get('/cadastrarBanda', ['as'=>'banda_artista.listar', 'uses'=>'BandaArtistaController@listarBandaArtista'] );

Route::get('/bandaEditar/{codBandArtist}', ['as'=>'banda_artista.editar', 'uses'=>'BandaArtistaController@editarBandaArtista']);

Route::put('/salvarEdicaoBandaArtista/{codBandArtist}', ['as'=>'banda_artista.update', 'uses'=>'BandaArtistaController@salvarEdicao']);

Route::get('excluirBandaArtista/{codBandArtist}', 'BandaArtistaController@excluirBandaArtista');
//Fecha banda artista


//Cadastrar album
Route::get('/cadastrarAlbum',['as'=>'album.listar','uses'=>'AlbumController@listarAlbum']);

Route::post('visualizarAlbum',['uses'=>'AlbumController@visualizarAlbum']);

Route::post('/salvarAlbum',['as'=>'album.salvar', 'uses'=>'AlbumController@salvar']);

Route::get('/albumEditar/{codAlbum}', ['as'=>'album.editar', 'uses'=>'AlbumController@editarAlbum']);

Route::put('/salvarEdicaoAlbum/{codAlbum}', ['as'=>'album.update', 'uses'=>'AlbumController@salvarEdicao']);

Route::get('excluirAlbum/{codAlbum}', 'AlbumController@excluirAlbum');

//Fecha album

// Faixa
Route::get('/cadastrarFaixa',['as'=>'faixa.listar','uses'=>'FaixaController@listarAlbumFaixa']);

Route::post('/salvarFaixa',['as'=>'faixa.salvar', 'uses'=>'FaixaController@salvar']);

Route::post('visualizarFaixa',['uses'=>'FaixaController@visualizarFaixa']);

Route::get('/faixaEditar/{codFaixa}', ['as'=>'faixa.editar', 'uses'=>'FaixaController@editarFaixa']);

Route::put('/salvarEdicaoFaixa/{codFaixa}', ['as'=>'faixa.update', 'uses'=>'FaixaController@salvarEdicao']);

Route::get('excluirFaixa/{codFaixa}', 'FaixaController@excluirFaixa');

//Faixa