<?php

use Illuminate\Database\Seeder;
use App\UsuarioModel;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $senha = md5('123@Mudar');
        $usuario = new UsuarioModel();
        $usuario->nome = "Marcelo Gomes";
        $usuario->password_usuario =$senha;
        $usuario->email = "marcelogn2010@hotmail.com";
        $usuario->caminho_imagem = "img_album/Folder.jpg";
        $usuario->permissao_usuario = 3;

        $usuario->save();
    }

}
