<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('usuario', function(Blueprint $table){
            $table->increments('codUser');
            $table->string('nome');
            $table->string('email');
            $table->string('password_usuario');
            $table->string('caminho_imagem');
            $table->integer('permissao_usuario');
            $table->rememberToken();
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('usuario');
        //
    }
}
