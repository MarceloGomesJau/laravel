<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlbumBandaArtista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('album_banda_artista', function(BluePrint $table){
            $table->increments('codAlbum');
            $table->integer('codBandaArtistaCad')->unsigned();
            $table->foreign('codBandaArtistaCad')->references('codBandArtist')->on('banda_artista');
            $table->string('nomeAlbum');
            $table->string('caminho_imagem_album');
            $table->Datetime('ano_lancamento');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
