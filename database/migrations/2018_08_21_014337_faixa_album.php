<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FaixaAlbum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('faixa_album', function(BluePrint $table){
            $table->increments('codFaixa');
            $table->integer('codAlbumCad')->unsigned();
            $table->foreign('codAlbumCad')->references('codAlbum')->on('album_banda_artista');
            $table->string('nome_faixa');
            $table->time('duracao');
            $table->string('compositor');
            $table->integer('numero_faixa');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
