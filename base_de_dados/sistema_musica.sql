﻿# Host: localhost  (Version 5.5.5-10.1.30-MariaDB)
# Date: 2018-08-23 23:42:42
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "banda_artista"
#

DROP TABLE IF EXISTS `banda_artista`;
CREATE TABLE `banda_artista` (
  `codBandArtist` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caminho_imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`codBandArtist`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "banda_artista"
#

INSERT INTO `banda_artista` VALUES (5,'Kiss','Rock','img_banda_artista/caminhos-da-vida-vol-7.jpg','Teste','2018-08-23 15:53:44','2018-08-23 15:53:44'),(6,'Korn Troca','Rock n Roll','img_banda_artista/Vol 3 frente.jpg','Testando Nova funcionalidade','2018-08-24 01:38:44','2018-08-24 01:39:00'),(7,'Banda Charlie Brown Jr Troca','Rock','img_banda_artista/thumb-6.jpg','Criada em 2018','2018-08-24 01:47:00','2018-08-24 01:47:14');

#
# Structure for table "album_banda_artista"
#

DROP TABLE IF EXISTS `album_banda_artista`;
CREATE TABLE `album_banda_artista` (
  `codAlbum` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codBandaArtistaCad` int(10) unsigned NOT NULL,
  `nomeAlbum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caminho_imagem_album` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ano_lancamento` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`codAlbum`),
  KEY `album_banda_artista_codbandaartistacad_foreign` (`codBandaArtistaCad`),
  CONSTRAINT `album_banda_artista_codbandaartistacad_foreign` FOREIGN KEY (`codBandaArtistaCad`) REFERENCES `banda_artista` (`codBandArtist`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "album_banda_artista"
#

INSERT INTO `album_banda_artista` VALUES (4,6,'Cd Skate Vibration Troca','img_album/image-4.jpg','2018-08-13 00:00:00','2018-08-24 01:48:30','2018-08-24 01:48:54');

#
# Structure for table "faixa_album"
#

DROP TABLE IF EXISTS `faixa_album`;
CREATE TABLE `faixa_album` (
  `codFaixa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codAlbumCad` int(10) unsigned NOT NULL,
  `nome_faixa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duracao` time NOT NULL,
  `compositor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_faixa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`codFaixa`),
  KEY `faixa_album_codalbumcad_foreign` (`codAlbumCad`),
  CONSTRAINT `faixa_album_codalbumcad_foreign` FOREIGN KEY (`codAlbumCad`) REFERENCES `album_banda_artista` (`codAlbum`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "faixa_album"
#

INSERT INTO `faixa_album` VALUES (9,4,'Na atividade Troca','04:00:00','Chorao',1,'2018-08-24 01:51:29','2018-08-24 01:51:49'),(10,4,'Skate Vibration','02:00:00','Marcelo',2,'2018-08-24 01:51:29','2018-08-24 01:51:29');

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (9,'2018_08_18_025957_table_usuario',1),(10,'2018_08_19_034123_banda_artista',1),(11,'2018_08_19_183531_album_banda_artista',1),(12,'2018_08_21_014337_faixa_album',2);

#
# Structure for table "usuario"
#

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `codUser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caminho_imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissao_usuario` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`codUser`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "usuario"
#

INSERT INTO `usuario` VALUES (1,'Marcelo Gomes','usuarioadm@hotmail.com','a6ac10b3b739e420edacb541488d6b1f','img_album/Folder.jpg',3,NULL,'2018-08-22 22:41:23','2018-08-24 01:54:35'),(3,'Adriano lopes Troca','teste@teste.com.br','a6ac10b3b739e420edacb541488d6b1f','img_usuario/caminhos-da-vida-vol-7.jpg',1,NULL,'2018-08-23 20:43:06','2018-08-23 20:43:40'),(4,'Usuario teste','usuario@usuario.com.br','a6ac10b3b739e420edacb541488d6b1f','img_usuario/Folder.jpg',2,NULL,'2018-08-23 20:49:26','2018-08-23 20:49:26');
