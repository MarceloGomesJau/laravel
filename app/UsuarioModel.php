<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;

class UsuarioModel extends Model
{
    //



    protected $table = "usuario";
    
    protected $fillable =[
        'nome', 'email', 'caminho_imagem', 'permissao_usuario'
    ];

    protected $hidden = array('password_senha');

    public function getAuthIdentifier(){
        return $this->getKey();
    }
    public function getAuthPassword(){
        return $this->password_senha;
    }

    public function getRememberTokenName(){
        return 'remember_token';
    }

    protected $primaryKey = 'codUser';

}
