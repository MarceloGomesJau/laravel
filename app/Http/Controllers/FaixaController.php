<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BandaArtistaModel;
use App\AlbumModel;
use App\faixaModel;
use DB;

class FaixaController extends Controller
{
    //
    public function listarAlbumFaixa(){
        $album = AlbumModel::join('banda_artista','codBandaArtistaCad','=', 'codBandArtist')->orderby('nomeAlbum')->get();
        $listarFaixaAlbum = faixaModel::join('album_banda_artista', 'codAlbumCad','=','codAlbum')->get();
        return view('cadastroFaixa', compact('album', 'listarFaixaAlbum'));
    }

    public function salvar(Request $request){
        $dadosEnviados = $request->all();

        
        
         $dadosEnviados['codAlbumCad'];
        
        for($i=0; $i< $dadosEnviados['valorFaixa']; $i++ )
        {
        
            $retorno = FaixaController::salvarFaixas($dadosEnviados['codAlbumCad'], $dadosEnviados['nomeMusica'.$i],
            $dadosEnviados['duracaoMusica'.$i], $dadosEnviados['compositorMusica'.$i], $dadosEnviados['numeroFaixa'.$i]);
            
        }
        return redirect()->route('faixa.listar');

    }

    public function salvarFaixas($codigoAlbum, $nome, $duracao, $compositor, $numero_faixa){
        $faixa = new faixaModel();
        $faixa->codAlbumCad = $codigoAlbum;
        $faixa->nome_faixa = $nome;
        $faixa->duracao = $duracao;
        $faixa->compositor = $compositor;
        $faixa->numero_faixa = $numero_faixa;

        $faixa->save();
    }

    public function editarFaixa($codigoFaixa){
        $registro = faixaModel::find($codigoFaixa);

        $album = AlbumModel::join('banda_artista','codBandaArtistaCad','=', 'codBandArtist')->orderby('nomeAlbum')->get();
        $listarFaixaAlbum = faixaModel::join('album_banda_artista', 'codAlbumCad','=','codAlbum')->get();
        return view('cadastroFaixa', compact('album', 'listarFaixaAlbum', 'registro'));
    }

    public function salvarEdicao(Request $request, $codigoFaixa){
        $dados = $request->all();
        $faixa =  faixaModel::find($codigoFaixa);
        $faixa->codAlbumCad = $dados['codAlbumCad'];
        $faixa->nome_faixa = $dados['nomeMusica'];
        $faixa->duracao = $dados['duracaoMusica'];
        $faixa->compositor = $dados['compositorMusica'];
        $faixa->numero_faixa = $dados['numeroFaixa'];

        $faixa->update();
        return redirect()->route('faixa.listar');
    }
    public function excluirFaixa($codFaixa){
        $delete = faixaModel::find($codFaixa)->delete();

        $response = array('status' => "Ok");
       
        return response()->json($response);
    }

    public function visualizarFaixa(Request $request){
        $dados = $request->all();
        $mostrarFaixa = DB::SELECT('select * from faixa_album where codFaixa = ?', [$dados['codigo']]);

        if($mostrarFaixa != null){
            $response = array('nome_faixa'=> $mostrarFaixa[0]->nome_faixa,
            "duracao" => $mostrarFaixa[0]->duracao,
            "numero_faixa" => $mostrarFaixa[0]->numero_faixa);
        }
        return response()->json($response);
    
    }
}
