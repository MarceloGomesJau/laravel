<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsuarioModel;
use DB;
use Auth;
use App\Http\Controllers\Session;
class UsuarioController extends Controller
{
    //
    public function listarUsuario(){

        $usuario = UsuarioModel::orderby('permissao_usuario')->get();
        
        return view('cadastroUsuario', compact('usuario'));
    }

    public function login(Request $request){
        $dados = $request->all();
        $email = $dados['email'];
        $senha = md5($dados['password']);
        $usuario = DB::select('select * From usuario where email = ? AND password_usuario = ?', [$email, $senha]);
        if($usuario != null){
            $request->session()->put(
                [
                    'nome'     => $usuario[0]->nome,
                    'permissao'             => $usuario[0]->permissao_usuario,
                ]
            );
            
        return redirect()->route('index.sistema');
        }else{
            return view('login');
        }
    }


    public function editarUsuario($id){

      
        $registro = UsuarioModel::find($id);
        $usuario = UsuarioModel::orderby('permissao_usuario')->get();

        return view('cadastroUsuario', compact('registro','usuario'));
    }

    public function salvarEdicao(Request $request, $codigoUsuario){
        $registro = UsuarioModel::find($codigoUsuario);

        $dados = $request->all();

        $registro->nome = $dados['nomeUsuario'];
        $registro->email =  $dados['emailUsuario'];
        $file = $request->file('imagemUsuario');
        if($file){
            $nomeArquivo = $file->getClientOriginalName();
            $diretorio = "img_usuario";
            $file->move($diretorio, $nomeArquivo);
            $registro->caminho_imagem = $diretorio."/".$nomeArquivo;
        }
        $registro->permissao_usuario = $dados['permissao'];
        $senha = UsuarioController::verificarSenha($dados['senhaUsuario'], $codigoUsuario);
        if($senha == null){
             $registro->password_usuario = md5($dados['senhaUsuario']);
        }

        
        $registro->update();

        return redirect()->route('usuario.listar');


    }
    public function verificarSenha($senha, $usuario){
        $achouSenha = DB::select('select codUser From usuario where codUser = ? AND password_usuario = ?', [$usuario, $senha]);

        return $achouSenha;

    }

    public function validarNome(Request $request){
        $dados = $request->all();
        $achouNome = DB::select('select * From usuario where nome = ?', [$dados['nome']]);

        if($achouNome != null){
            $response = array('status' => "Ok");
        }else{
            $response = array('status' => "Erro");
        }
        return response()->json($response);
    }

    public function validarEmail(Request $request){
        $dados = $request->all();
        $achouEmail = DB::select('select * From usuario where email = ?', [$dados['email']]);

        if($achouEmail != null){
            $response = array('status' => "Ok");
        }else{
            $response = array('status' => "Erro");
        }
        return response()->json($response);
    }
    

    public function excluirUsuario($codUsuario){


       $delete = UsuarioModel::find($codUsuario)->delete();

       $response = array('status' => "Ok");
       
       return response()->json($response);
    }
    public function salvar(Request $request){
        $dadosEnviados = $request->all();


        $usuario = new UsuarioModel();

        $usuario->nome = $dadosEnviados['nomeUsuario'];
        $usuario->email = $dadosEnviados['emailUsuario'];
        
        $file = $request->file('imagemUsuario');
        $nomeArquivo = $file->getClientOriginalName();
        $diretorio = "img_usuario";
        $file->move($diretorio, $nomeArquivo);
        $usuario->caminho_imagem = $diretorio."/".$nomeArquivo;

        $usuario->permissao_usuario  = $dadosEnviados['permissao'];
        $usuario->password_usuario = md5($dadosEnviados['senhaUsuario']);

        $usuario->save();

        return redirect()->route('usuario.listar');
    }

    public function sair(){

        session()->flush();
        return redirect()->route('/');
    }
}
