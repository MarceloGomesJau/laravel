<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BandaArtistaModel;
use App\faixaModel;
use App\AlbumModel;
use DB;
class BandaArtistaController extends Controller
{
    
    public function listarBandaArtista(){

        $bandaArtista = BandaArtistaModel::orderby('nome')->get();
        
        return view('cadastroBandaArtista', compact('bandaArtista'));
    }

    public function editarBandaArtista($id){

      
        $registro = BandaArtistaModel::find($id);
        $bandaArtista = BandaArtistaModel::orderby('nome')->get();

        return view('cadastroBandaArtista', compact('registro','bandaArtista'));
    }

    public function salvarEdicao(Request $request, $codigoBandaArtista){
        $registro = BandaArtistaModel::find($codigoBandaArtista);

        $dados = $request->all();
        $registro->nome = $dados['nomeBandaArtista'];
        $registro->genero =  $dados['generoBandaArtista'];
        $file = $request->file('imagemBandaArtista');
        if($file){
            $nomeArquivo = $file->getClientOriginalName();
            $diretorio = "img_banda_artista";
            $file->move($diretorio, $nomeArquivo);
            $registro->caminho_imagem = $diretorio."/".$nomeArquivo;
        }
        $registro->descricao = $dados['descricaoBandaArtista'];
        
        $registro->update();

        return redirect()->route('banda_artista.listar');


    }

    public function excluirBandaArtista($codBandaArtista){

       $apagarAlbum = BandaArtistaController::apagarAlbum($codBandaArtista);
       $delete = BandaArtistaModel::find($codBandaArtista)->delete();

       $response = array('status' => "Ok");

       return response()->json($response);
    }

     public function apagarAlbum($codigoBandaArtista){
        
        $apagar = DB::table('album_banda_artista')->where('codBandaArtistaCad', '=', $codigoBandaArtista)->delete();

         return true;
     }
    public function salvar(Request $request){
        $dadosEnviados = $request->all();


        $bandaArtista = new BandaArtistaModel();

        $bandaArtista->nome = $dadosEnviados['nomeBandaArtista'];
        $bandaArtista->genero = $dadosEnviados['generoBandaArtista'];
        
        $file = $request->file('imagemBandaArtista');
        $nomeArquivo = $file->getClientOriginalName();
        $diretorio = "img_banda_artista";
        $file->move($diretorio, $nomeArquivo);
        $bandaArtista->caminho_imagem = $diretorio."/".$nomeArquivo;

        $bandaArtista->descricao  = $dadosEnviados['descricaoBandaArtista'];

        $bandaArtista->save();

        return redirect()->route('banda_artista.listar');
    }

    public function validarBandaArtista(Request $request){
        $dados = $request->all();
        $achouBanda = DB::select('select * From banda_artista where nome = ?', [$dados['banda']]);

        if($achouBanda != null){
            $response = array('status' => "Ok");
        }else{
            $response = array('status' => "Erro");
        }
        return response()->json($response);
    }

    public function visualizarBanda(Request $request){
        $dados = $request->all();
        $mostrarBanda = DB::SELECT("select * from banda_artista WHERE codBandArtist = ?", [$dados['codigo']]);

        if($mostrarBanda != null){
            $response = array('nome_banda'=> $mostrarBanda[0]->nome,
            "genero" => $mostrarBanda[0]->genero,
            "descricao" => $mostrarBanda[0]->descricao);
        }
        return response()->json($response);
    }


}
