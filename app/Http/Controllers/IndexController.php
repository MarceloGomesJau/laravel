<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\faixaModel;
use App\AlbumModel;
use App\BandaArtistaModel;

class IndexController extends Controller
{
    //
    public function mostrar(){
        $faixa = faixaModel::count();
        $album = AlbumModel::count();
        $bandaArtista = BandaArtistaModel::count();
        
        return view('index', compact('faixa','album','bandaArtista'));
    }
}
