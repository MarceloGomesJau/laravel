<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BandaArtistaModel;
use App\AlbumModel;
use App\faixaModel;
use DB;
class AlbumController extends Controller
{
    //
    public function listarAlbum(){

        $album = AlbumModel::join('banda_artista', 'codBandaArtistaCad', '=', 'codBandArtist')->get();       
        $banda = BandaArtistaModel::orderby('nome')->get();
        return view('cadastrarAlbum', compact('album', 'banda'));
    }

    public function editarAlbum($codAlbum){

        
        $registro = AlbumModel::find($codAlbum);
        $album = AlbumModel::join('banda_artista', 'codBandaArtistaCad', '=', 'codBandArtist')->get();       
        $banda = BandaArtistaModel::orderby('nome')->get();

        return view('cadastrarAlbum', compact('registro','album','banda'));
    }

    public function salvarEdicao(Request $request, $codigoAlbum){
        $registro = AlbumModel::find($codigoAlbum);

        $dados = $request->all();
        $registro->nomeAlbum = $dados['nomeAlbum'];
        $registro->codBandaArtistaCad = $dados['codBandaArtista'];
        $file = $request->file('imagemAlbum');
        if($file){
            $nomeArquivo = $file->getClientOriginalName();
            $diretorio = "img_album";
            $file->move($diretorio, $nomeArquivo);
            $registro->caminho_imagem_album = $diretorio."/".$nomeArquivo;
        }
        $registro->ano_lancamento = $dados['dataLancamento'];
        
        $registro->update();

        return redirect()->route('album.listar');


    }

    
    public function excluirAlbum($codAlbum){

        $apagarAlbum = AlbumController::apagarFaixa($codAlbum);
        $delete = AlbumModel::find($codAlbum)->delete();
 
        $response = array('status' => "Ok");
       
        return response()->json($response);
     }

     public function apagarFaixa($codAlbum){
         $deleteFaixa = faixaModel::Where('codAlbumCad', '=', $codAlbum)->delete();
         return true;
     }

    public function salvar(Request $request){
        $dadosEnviados = $request->all();


        $album = new AlbumModel();

        $album->nomeAlbum = $dadosEnviados['nomeAlbum'];
        $album->codBandaArtistaCad = $dadosEnviados['codBandaArtista'];
        
        $file = $request->file('imagemAlbum');
        $nomeArquivo = $file->getClientOriginalName();
        $diretorio = "img_album";
        $file->move($diretorio, $nomeArquivo);
        $album->caminho_imagem_album = $diretorio."/".$nomeArquivo;

        $album->ano_lancamento  = $dadosEnviados['dataLancamento'];

        $album->save();

        return redirect()->route('album.listar');
    }

    public function visualizarAlbum(Request $request){
        $dados = $request->all();
        $mostrarAlbum = DB::SELECT('select * from album_banda_artista WHERE codAlbum = ?', [$dados['codigo']]);

        if($mostrarAlbum != null){
            $response = array('nome_album'=> $mostrarAlbum[0]->nomeAlbum,
            "ano_lancamento" => date('d/m/Y', strtotime($mostrarAlbum[0]->ano_lancamento)),
           );
        }
        return response()->json($response);
    }

}
