<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumModel extends Model
{
    //
    protected $table = "album_banda_artista";


    protected $fillable = ['nomeAlbum', 'caminho_imagem_album', 'ano_lancamento'];

    protected $primaryKey = 'codAlbum';
    

     //data formatada m-d-Y
     protected $appends = ['data'];
     public function getData2Attribute()
     {
         return date('d-m-Y', strtotime($this->attributes['ano_lancamento']));
     }
}
