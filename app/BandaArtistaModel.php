<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BandaArtistaModel extends Model
{
    //
    protected $table = "banda_artista";

    protected $fillable =[
        'nome', 'genero', 'caminho_imagem','descricao'
    ];

    protected $primaryKey = 'codBandArtist';
}
