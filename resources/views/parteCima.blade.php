<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle pull-left">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                @if(session('permissao') == 1) 
                    Público
                @elseif(session('permissao') == 2)
                    Usuário
                @else   
                    Administrador
                @endif
                
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('login.sair') }}">Sair</a></li>
            </ul>
        </div>
    </div>
</nav>