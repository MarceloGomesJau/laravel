<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Dashboard</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
        <script>
				$(document).ready(function(){
				$('#tabelaFaixa').dataTable( {
					"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
					language: {
						processing:     "Processando...",
						search:         "<span style='color:#296192'>Pesquisar:</span>",
						lengthMenu:    "<span style='color:#296192'> Exibindo  _MENU_ por página </span> ",
						info:           "Exibindo de _START_ a _END_  com o total de _TOTAL_ elementos",
						infoEmpty:      "Nenhum registro encontrado com esse filtro",
						infoFiltered:   "(Filtrado de  _MAX_ elementos no total)",
						infoPostFix:    "",
						loadingRecords: "Carregando dados...",
						zeroRecords:    "0 Registros",
						emptyTable:     "Tabela vazia",
						paginate: {
							first:      "Primeiro",
							previous:   "Anterior",
							next:       "Próximo",
							last:       "Último"
						},
						aria: {
							sortAscending:  ": Ordenado por ordem crescente",
							sortDescending: ": Ordenado por ordem decrescente"
						}
					}
				} );
			});


		</script>
</head>

<body>
	@include('parteCima')
	<div class="main">
		@include('menu')
		<div class="content">
			<div class="container-fluid">
                
                    @if(!isset($registro->codFaixa))
                        <form action="{{route('faixa.salvar')}}" method="POST" enctype="multipart/form-data">
                    @else
                        <form action="{{route('faixa.update', $registro->codFaixa)}}" method="POST" enctype="multipart/form-data">
                    @endif
                        {{csrf_field()}}
                        @if(session('permissao') == 2 || session('permissao') == 3)
                        <div class="row">
                            @if(!isset($registro->codFaixa))
                                <h3 align="center">Cadastro Faixa</h3>
                                <h3 align="left">Primeiro selecione o album</h3>
                            @else
                                <h3 align="center">Editar Faixa</h3>
                            @endif
                            <br/>
                            <div class="col-sm-8">
                                <label for="">Album</label>
                                
                                <select name="codAlbumCad" id="codAlbumCad" class="form-control" required="required" onchange="selecionarBanda()">
                                <option value="Selecione" >Selecione</option>
                                    @foreach($album as $value)
                                    <option value="{{$value->codAlbum}}" {{isset($registro->codAlbumCad) && $registro->codAlbumCad == $value->codAlbum ? 'selected' : '' }}>{{$value->nomeAlbum}} - {{$value->nome}}</option>
                                    @endforeach

                                </select>
                                
                            </div>

                        </div>
                        <div class="row" style="display:none" id="mostrarDivFaixa">
                            <label for="">Selecione quantas faixa tem o album</label>
                            
                            <input type="number" name="valorFaixa" id="valorFaixa" class="form-control" title="" >
                            <br/>
                            <button type="button" name="botaoGerar" id="botaoGerar" class="btn btn-success" onclick="adicionarFaixas()">Gerar Faixas</button>
                            
                        </div>
                        <div class="row" style="display:none" id="addInf">
                        </div>
                        @if(isset($registro->codFaixa))
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="nomeMusica">Nome da musica</label>
                                <input type="text" name="nomeMusica" id="nomeMusica" value="{{$registro->nome_faixa}}"/>   
                            </div>
                            <div class="col-sm-4">
                                <label for="compositorMusica">Compositor(a)</label>
                                <input type="text" name="compositorMusica" id="compositoroMusica" value="{{$registro->compositor}}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="duracaoMusica">Duração da musica</label>
                                <input type="time" name="duracaoMusica" id="duracaoMusica" value="{{$registro->duracao}}"/>
                            </div>   

                            <div class="col-sm-2">
                                <label for="Numero">Numero da Faixa </label>
                                <input type="text" name="numeroFaixa" id="numeroFaixa" value="{{$registro->numero_faixa}}"/>                        
                            </div>  
                        </div>
                        @endif
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">
                                @if(isset($registro->codFaixa))
                                    <input type="hidden" name="_method" value="put">
                                    <button type="submit" class="btn btn-primary">Editar informação</button>
                                @else
                                    <button type="submit" class="btn btn-primary">Enviar informação</button>
                                @endif
                            </div>
                        </div>
                    @endif
                    </form>
               
                <br/>
				<div class="row">
					
                    <div class="table-responsive">
                            <table class="table table-hover" id="tabelaFaixa">
                                <thead>
                                    <tr>
                                        <th class="text-algin:center">Código da faixa</th>
                                        <th class="text-align:center">Imagem do album</th>
                                        <th class="text-align:center">Nome do Album</th>
                                        <th class="text-align:center">Nome da faixa</th>
                                        <th class="text-align:center">Duração</th>
                                        <th class="text-align:center">Compositor</th>
                                        <th class="text-align:center">Número da faixa</th>
                                        <th class="text-align:center">Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($listarFaixaAlbum as $faixa)
                                    <tr>
                                        <td class="text-align:center">{{$faixa->codFaixa}}</td>
                                        <td class="text-align:center"><img src="{{asset($faixa->caminho_imagem_album)}}" width="10%"></td>
                                        <td class="text-align:center">{{$faixa->nomeAlbum}}</td>
                                        <td class="text-align:center">{{$faixa->nome_faixa}}</td>
                                        <td class="text-align:center">{{date('H:i', strtotime($faixa->duracao))}}</td>
                                        <td class="text-align:center">{{$faixa->compositor}}</td>
                                        <td class="text-align:center">{{$faixa->numero_faixa}}</td>
                                        <td class="text-align:center">
                                            <a class="btn btn-primary" onclick="visualizarFaixa({{$faixa->codFaixa}})">Visualizar</a>
                                            @if(session('permissao') == 3 || session('permissao') ==2)
                                                <a class="btn btn-success" href="{{route ('faixa.editar', $faixa->codFaixa)}}">Editar</a>
                                                <button type="button" class="btn btn-danger" onclick="excluirFaixa({{$faixa->codFaixa}})">Excluir</button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
			</div>
		</div>
	</div>


    
</body>

<div class="modal fade" id="modal-faixa">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Visualizar faixa</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Faixa :<strong><span id="nomeFaixa"></span></strong></p>
                        <p>Duração : <strong><span id="duracao"></span></strong></p>
                        <p>Número da faixa :<strong><span id="numeroFaixa"></span></strong></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


</html>