<div class="menu">
    <ul>
        <li class="visible-xs"><a href="#">Sair</a></li>
        <li class="active"><a href="#">Painel</a></li>
        @if(session('permissao') == 3)
        <li><a href="{{route('usuario.listar') }}">Usuarios</a></li>
        @endif
        <li><a href="{{route('banda_artista.listar')}}">Bandas Artistas</a></li>
        <li><a href="{{route('album.listar')}}">Album</a></li>
        <li><a href="{{route('faixa.listar')}}">Músicas</a></li>
    </ul>
</div>