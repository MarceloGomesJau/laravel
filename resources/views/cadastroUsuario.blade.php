<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Dashboard</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
            $(document).ready(function(){
            $('#tabelaUsuario').dataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                language: {
                    processing:     "Processando...",
                    search:         "<span style='color:#296192'>Pesquisar:</span>",
                    lengthMenu:    "<span style='color:#296192'> Exibindo  _MENU_ por página </span> ",
                    info:           "Exibindo de _START_ a _END_  com o total de _TOTAL_ elementos",
                    infoEmpty:      "Nenhum registro encontrado com esse filtro",
                    infoFiltered:   "(Filtrado de  _MAX_ elementos no total)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando dados...",
                    zeroRecords:    "0 Registros",
                    emptyTable:     "Tabela vazia",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Próximo",
                        last:       "Último"
                    },
                    aria: {
                        sortAscending:  ": Ordenado por ordem crescente",
                        sortDescending: ": Ordenado por ordem decrescente"
                    }
                }
            } );
        });


    </script>
</head>

<body>
	@include('parteCima')
	<div class="main">
		@include('menu')
		<div class="content">
			<div class="container-fluid">
					@if(!isset($registro->codUser))
						
						<form action="{{route('usuario.salvar')}}" method="POST" enctype="multipart/form-data">
					@else
						<form action="{{route('usuario.update', $registro->codUser)}}" method="POST" enctype="multipart/form-data">
					@endif
						{{csrf_field()}}
						<div class="row">
							@if(!isset($registro->codUser))
								<h3 align="center">Cadastro usuário</h3>
								
							@else
								<h3 align="center">Editar usuario</h3>
							@endif
							<br/>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6 ">
								<label for="">Nome de usuario</label>
								<input type="text" name="nomeUsuario" id="nomeUsuario" class="form-control" value="{{isset($registro->nome) ? $registro->nome : ''}}" required="required"  title="" onblur="validarNome()">
							</div>
							<div class="col-sm-6">
								<label for="">Email usuario</label>
								<input type="text" name="emailUsuario" id="emailUsuario" class="form-control" value="{{isset($registro->email) ? $registro->email : ''}}" required="required"  title="" onblur="validarEmail()">
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								<label for="">Imagem usuario</label>
								<input type="file" class="form-control-file" name="imagemUsuario" id="imagemUsuario" placeholder="" aria-describedby="fileHelpId">
								<small id="fileHelpId" class="form-text text-muted">Imagem do usuario</small>
								<br/>
								@if(isset($registro->caminho_imagem))
									<img src="{{asset($registro->caminho_imagem)}}" width="120px" srcset="">
								@endif
							</div>
							<div class="col-sm-6">
								<label for="">Senha usuario</label>
								<input type="password" name="senhaUsuario" id="senhaUsuario" class="form-control" value="{{isset($registro->password_usuario) ? $registro->password_usuario : ''}}" required="required"  title="">
								<br/>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								<label for="">Permissão usuario</label>
								
								<select name="permissao" id="permissao" class="form-control" required="required" onchange="mostrarTexto(this.value)">
									<option value="1" {{isset($registro->codUser) && $registro->permissao_usuario == 1 ? 'selected': ''}}> Publico</option>
									<option value="2" {{isset($registro->codUser) && $registro->permissao_usuario == 2 ? 'selected': ''}}> Usuario</option>
									<option value="3" {{isset($registro->codUser) && $registro->permissao_usuario == 3 ? 'selected': ''}}> Admin</option>
								</select>
								
							</div>
							<div class="col-sm-6">
								<div id="mostrarPublico" style="display:none">
									<p id="mostrarPublico" >O perfil publico pode apenas visualizar e listar Bandas ou artistas, album e música</p>
									<p>Nenhuma inserção, alteração ou exclusão o usuário irá fazer</p>
								</div>
								
								<div id="mostrarUsuario" style="display:none">
									<p>O perfil usuário pode visualizar  listar, editar e excluir Bandas ou artistas, album e música</p>
									<p>Não tem acesso ao cadastro de usuário</p>
								</div>

								
								<div id="mostrarAdmin" style="display:none">
									<p>O perfil administrador pode editar, visualizar, excluir e listar Bandas ou artistas, albuns, usuários e música</p>
									<p>Tem acesso ao sistema todo</p>
								</div>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								@if(isset($registro->codUser))
									<input type="hidden" name="_method" value="put">
									<button type="submit" class="btn btn-primary">Editar informação</button>
								@else
									<button type="submit" class="btn btn-primary">Enviar informação</button>
								@endif
							</div>
						
						</div>
						<br/>
						<div class="row">

						</div>
					</form>
				
					<div class="row">
					
					<div class="table-responsive">
						<table class="table table-hover" id="tabelaUsuario">
							<thead>
								<tr>
									<th class="text-align:center">Código</th>
									<th class="text-align:center">Imagem</th>
									<th class="text-align:center">Nome</th>
									<th class="text-align:center">Email</th>
									<th class="text-align:center">Permissão</th>
									<th class="text-align:center">Ação</th>
								</tr>
							</thead>
							<tbody>
								@foreach($usuario as $user)
								<tr>
									<td class="text-align:center">{{$user->codUser}}</td>
									<td class="text-align:center"><img src="{{asset($user->caminho_imagem)}}" width="10%"></td>
									<td class="text-align:center">{{$user->nome}}</td>
									<td class="text-align:center">{{$user->email}}</td>
									<td class="text-align:center">
										@if($user->permissao_usuario == 1)
											Publico
										@elseif($user->permissao_usuario == 2)
											Usuario
										@else
											Administrador
										@endif
									</td>
									<td class="text-align:center">
										<a class="btn btn-success" href="{{route ('usuario.editar', $user->codUser)}}">Editar</a>
										<button type="button" class="btn btn-danger" onclick="excluirUsuario({{$user->codUser}})">Excluir</button>
										
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</body>

</html>