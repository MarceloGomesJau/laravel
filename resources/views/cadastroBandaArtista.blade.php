<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Dashboard</title>
	<script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/script.js')}}"></script> 
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<!-- Optional theme -->

<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
            $(document).ready(function(){
            $('#tabelaBandaArtista').dataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                language: {
                    processing:     "Processando...",
                    search:         "<span style='color:#296192'>Pesquisar:</span>",
                    lengthMenu:    "<span style='color:#296192'> Exibindo  _MENU_ por página </span> ",
                    info:           "Exibindo de _START_ a _END_  com o total de _TOTAL_ elementos",
                    infoEmpty:      "Nenhum registro encontrado com esse filtro",
                    infoFiltered:   "(Filtrado de  _MAX_ elementos no total)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando dados...",
                    zeroRecords:    "0 Registros",
                    emptyTable:     "Tabela vazia",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Próximo",
                        last:       "Último"
                    },
                    aria: {
                        sortAscending:  ": Ordenado por ordem crescente",
                        sortDescending: ": Ordenado por ordem decrescente"
                    }
                }
            } );
        });


    </script>
	
	
</head>

<body>
	@include('parteCima')
	<div class="main">
		@include('menu')
		<div class="content">
			<div class="container-fluid">
					@if(!isset($registro->codBandArtist))
						<form action="{{route('banda_artista.salvar')}}" method="POST" enctype="multipart/form-data">
					@else
						<form action="{{route('banda_artista.update', $registro->codBandArtist)}}" method="POST" enctype="multipart/form-data">
					@endif
				
						
						{{csrf_field()}}
						@if(session('permissao') == 2 || session('permissao') == 3)
				
						<div class="row">
							@if(!isset($registro->codBandArtist))
								<h3 align="center">Cadastro banda ou artista</h3>
								
							@else
								<h3 align="center">Editar banda ou artista</h3>
							@endif
							<br/>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6 ">
								<label for="">Nome de banda ou artista</label>
								<input type="text" name="nomeBandaArtista" id="nomeBandaArtista" class="form-control" value="{{isset($registro->nome) ? $registro->nome : ''}}" required="required"  title="" onblur="validarBandaArtista()">
							</div>
							<div class="col-sm-6">
								<label for="">Genero tocado</label>
								<input type="text" name="generoBandaArtista" id="generoBandaArtista" class="form-control" value="{{isset($registro->genero) ? $registro->genero : ''}}" required="required"  title="">
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								<label for="">Imagem da banda</label>
								<input type="file" class="form-control-file" name="imagemBandaArtista" id="imagemBandaArtista" placeholder="" aria-describedby="fileHelpId">
								<small id="fileHelpId" class="form-text text-muted">Imagem da banda</small>
								<br/>
								@if(isset($registro->caminho_imagem))
									<img src="{{asset($registro->caminho_imagem)}}" width="120px" srcset="">
								@endif
							</div>
							<div class="col-sm-6">
								<label for="">Descrição</label>
								<input type="text" name="descricaoBandaArtista" id="descricaoBandaArtista" class="form-control" value="{{isset($registro->descricao) ? $registro->descricao : ''}}" required="required"  title="">
								<br/>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								@if(isset($registro->codBandArtist))
									<input type="hidden" name="_method" value="put">
									<button type="submit" class="btn btn-primary">Editar informação</button>
								@else
									<button type="submit" class="btn btn-primary">Enviar informação</button>
								@endif
							</div>
						</div>
						@endif
					</form>
				
				<br/>
				<div class="row">
					
					<div class="table-responsive">
						<table class="table table-hover" id="tabelaBandaArtista">
							<thead>
								<tr>
									<th class="text-align:center">Código</th>
									<th class="text-align:center">Imagem</th>
									<th class="text-align:center">Nome</th>
									<th class="text-align:center">Genero</th>
									<th class="text-align:center">Descrição</th>
									<th class="text-align:center">Ação</th>
								</tr>
							</thead>
							<tbody>
								@foreach($bandaArtista as $value)
								<tr>
									<td class="text-align:center">{{$value->codBandArtist}}</td>
									<td class="text-align:center"><img src="{{asset($value->caminho_imagem)}}" width="10%"></td>
									<td class="text-align:center">{{$value->nome}}</td>
									<td class="text-align:center">{{$value->genero}}</td>
									<td class="text-align:center">{{$value->descricao}}</td>
									<td class="text-align:center">
										<a class="btn btn-primary" onclick="visualizarBanda({{$value->codBandArtist}})">Visualizar</a>
										@if(session('permissao') == 3 || session('permissao') ==2)
											<a class="btn btn-success" href="{{route ('banda_artista.editar', $value->codBandArtist)}}">Editar</a>
								
											<button type="button" class="btn btn-danger" onclick="excluirBandaArtista({{$value->codBandArtist}})">Excluir</button>
										@endif
										
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>	

</body>
<div class="modal fade" id="modal-id">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Visualização da banda</strong></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<p>Nome banda :<strong><span id="nomeBanda"></span></strong></p>
						<p>Genêro : <strong><span id="generoBanda"></span></strong></p>
						<p>Descrição :<strong><span id="descricao"></span></strong></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>

</html>