<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Dashboard</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
	@include('parteCima')
	<div class="main">
		@include('menu')
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-4 text-center">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">Quantidade de bandas cadastradas</h3>
							</div>
							
							<div class="panel-body">
								@if(isset($bandaArtista))
									<h3>{{$bandaArtista}}</h3>
								@else
									<h3>Nada cadastrado</h3>
								@endif
							</div>
						</div>
					</div>
					<div class="col-sm-4 text-center">
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Quantidade de músicas cadastradas</h3>
							</div>
							<div class="panel-body">
								@if(isset($faixa))
									<h3>{{$faixa}}</h3>
								@else
									<h3>Nada cadastrado</h3>
								@endif
							</div>
						</div>
					</div>
					<div class="col-sm-4 text-center">
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title">Quantidade de albuns cadastrados</h3>
							</div>
							<div class="panel-body">
								@if(isset($album))
									<h3>{{$album}}</h3>
								@else
									<h3>Nada cadastrado</h3>
								@endif
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</div>

	<script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
</body>

</html>