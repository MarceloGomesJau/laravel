<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Dashboard</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery-3.1.1.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script>
            $(document).ready(function(){
            $('#tabelaAlbum').dataTable( {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                language: {
                    processing:     "Processando...",
                    search:         "<span style='color:#296192'>Pesquisar:</span>",
                    lengthMenu:    "<span style='color:#296192'> Exibindo  _MENU_ por página </span> ",
                    info:           "Exibindo de _START_ a _END_  com o total de _TOTAL_ elementos",
                    infoEmpty:      "Nenhum registro encontrado com esse filtro",
                    infoFiltered:   "(Filtrado de  _MAX_ elementos no total)",
                    infoPostFix:    "",
                    loadingRecords: "Carregando dados...",
                    zeroRecords:    "0 Registros",
                    emptyTable:     "Tabela vazia",
                    paginate: {
                        first:      "Primeiro",
                        previous:   "Anterior",
                        next:       "Próximo",
                        last:       "Último"
                    },
                    aria: {
                        sortAscending:  ": Ordenado por ordem crescente",
                        sortDescending: ": Ordenado por ordem decrescente"
                    }
                }
            } );
        });


    </script>
</head>

<body>
	@include('parteCima')
	<div class="main">
		@include('menu')
		<div class="content">
			<div class="container-fluid">
				
					@if(!isset($registro->codAlbum))
						<form action="{{route('album.salvar')}}" method="POST" enctype="multipart/form-data">
					@else
						<form action="{{route('album.update', $registro->codAlbum)}}" method="POST" enctype="multipart/form-data">
					@endif
						{{csrf_field()}}
						@if(session('permissao') == 2 || session('permissao') == 3)
						<div class="row">
							@if(!isset($registro->codAlbum))
								<h3 align="center">Cadastro album</h3>
								
							@else
								<h3 align="center">Editar album</h3>
							@endif
							<br/>
						</div>
						<div class="row">
							<div class="col-sm-6 ">
								<label for="">Nome do album</label>
								<input type="text" name="nomeAlbum" id="nomeAlbum" class="form-control" value="{{isset($registro->nomeAlbum) ? $registro->nomeAlbum : ''}}" required="required"  title="">
							</div>
							<div class="col-sm-6">
								<label for="">Banda ou Artista</label>
								
								<select name="codBandaArtista" id="codBandaArtista" class="form-control" required="required">
								
									@foreach($banda as $band)
									<option value="{{$band->codBandArtist}}" {{isset($registro->codBandaArtistaCad) && $registro->codBandartistaCad == $band->codBandaArtista ? 'selected' : '' }}>{{$band->nome}}</option>
									@endforeach
								</select>
								
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								<label for="">Imagem do album</label>
								<input type="file" class="form-control-file" name="imagemAlbum" id="imagemAlbum" placeholder="" aria-describedby="fileHelpId">
								<small id="fileHelpId" class="form-text text-muted">Imagem do album</small>
								<br/>
								@if(isset($registro->caminho_imagem_album))
									<img src="{{asset($registro->caminho_imagem_album)}}" width="120px" srcset="">
								@endif
							</div>
							<div class="col-sm-6">
								<label for="">Ano de lançamento do album</label>
								<input type="date" name="dataLancamento" id="dataLancamento" class="form-control" value="{{isset($registro->ano_lancamento) ? date('Y-m-d', strtotime($registro->ano_lancamento)) : ''}}" required="required"  title="">
								<br/>
							</div>
						</div>
						<br/>
						<div class="row">
							<div class="col-sm-6">
								@if(isset($registro->codAlbum))
									<input type="hidden" name="_method" value="put">
									<button type="submit" class="btn btn-primary">Editar informação</button>
								@else
									<button type="submit" class="btn btn-primary">Enviar informação</button>
								@endif
							</div>
						</div>
						@endif
					</form>
				
                <br/>
				<div class="row">
					
					<div class="table-responsive" >
						<table class="table table-hover" id="tabelaAlbum">
							<thead>
								<tr>
									<th class="text-align:center">Código</th>
									<th class="text-align:center">Imagem do album</th>
									<th class="text-align:center">Nome do artista ou banda</th>
                                    <th class="text-align:center">Nome do album</th>
									<th class="text-align:center">Ano de lançamento do album</th>
                                    <th class="text-align:center">Descrição</th>
									<th class="text-align:center">Ação</th>
								</tr>
							</thead>
							<tbody>
								@foreach($album as $value)
								<tr>
									<td class="text-align:center">{{$value->codAlbum}}</td>
									<td class="text-align:center"><img src="{{asset($value->caminho_imagem_album)}}" width="10%"></td>
                                    <td class="text-align:center">{{$value->nome}}</td>
									<td class="text-align:center">{{$value->nomeAlbum}}</td>
									<td class="text-align:center">{{ date( 'd/m/Y' , strtotime($value->ano_lancamento))}}</td>
									<td class="text-align:center">{{$value->descricao}}</td>
									<td class="text-align:center">
									<a class="btn btn-primary" onclick="visualizarAlbum({{$value->codAlbum}})">Visualizar</a>
										@if(session('permissao') == 2 || session('permissao') == 3)
											<a class="btn btn-success" href="{{route ('album.editar', $value->codAlbum)}}">Editar</a>
										
											<button type="button" class="btn btn-danger" onclick="excluirAlbum({{$value->codAlbum}})">Excluir</button>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>			
				</div>
			</div>
		</div>
	</div>

</body>
<div class="modal fade" id="modal-album">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Visualizar album</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Nome do album :<strong><span id="nomeAlbumCad"></span></strong></p>
                        <p>Ano de lancamento : <strong><span id="anoLancamento"></span></strong></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

</html>